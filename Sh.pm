package Pl::Sh;
use Data::Dumper;

use File::Path qw(make_path remove_tree);

use File::Spec::Functions qw(rel2abs catfile catdir);

use File::Copy qw(cp mv);

use File::Basename;

use Cwd;
use strict qw(subs vars);
use warnings;

our $Pipers;

BEGIN {    $Pipers = {
        '+' => \&pipe_add, 
        cond => \&pipe_cond
    };
}



our $Root = File::Spec->rootdir();
sub pipe_add{
    my ($input, $arg) = @_;
    return $input + $arg;
}


sub pipe_cond {
    my ($input, $op, $arg) = @_;
    my %Conds = (
        '>' => sub { ($_[0] > $_[1]) ? $_[0] : undef },
        '<' => sub { ($_[0] < $_[1]) ? $_[0] : undef },
    );
    return (exists $Conds{$op})
        ? $Conds{$op}->($input, $arg)
        : die "Err: cond op $op not implementd"
        ;
}



sub seq {
    my @args = @_; 

    bless (sub {
        (my $v = shift @args);
        return ((defined $v)  )
            ? ($v)
            : ();
    }, 'SEQ');
}



sub filter_seq {
    my @lines = @_;

    return  sub {
        my $iterator = shift;
        my $rec; $rec = sub {
            my ($input, $topline) = (shift, shift);
            if($topline){
                my ($op, @rest) = @$topline;
                my $fun = (exists $Pipers->{$op})
                    ? $Pipers->{$op}
                    : die "Err (filter) op $op not available"
                    ;
                my $res = $fun->($input, @rest);
                $rec->($res, @_);
            }else{
                return $input;
            }
        };
        my (@nextinput) = $iterator->();
        if(@nextinput){
            return $rec->(@nextinput, @lines);
        }
        else{
            return () 
        }
    } ;

}

sub filter_list{ 
    my @lines = @_;

    sub {
        my ($list) = shift;
        my $rec; $rec = sub {
            my ($input, $cmdline) = (shift, shift);
            if($cmdline){
                my ($op, @rest) = @$cmdline;
                my $fun = (exists $Pipers->{$op})
                    ? $Pipers->{$op}
                    : die "Err (filter) op $op not available"
                    ;
                my $res = $fun->($input, @rest);
                $rec->($res, @_);
            }else{
                return $input;
            }
        };
        [ grep { $rec->($_, @lines) } @$list ] ;
    }}
sub filter {
    my $input = shift;
    
    my @lines = map {
        if((ref $_ eq 'ARRAY') or (ref $_ eq 'Regexp')){
            $_
        }elsif( ref \$_ eq 'SCALAR'){
            [ split ' ', $_  ]
        }else{ 
            die "Err: invalid elements in filter";
        } } @_;

    my  $seq_accu =   sub {
        my ($lazyarg, $pipe ) = @_;
        my $rec; $rec = sub {
            my (@v) = $pipe->($lazyarg);
            return unless @v;
            ($v[0]) ? ($v[0], $rec->()) : $rec->();
        };
        return [  $rec->() ] ;
    };
    my  $list_accu =   sub {
        my ($arg, $pipe) = @_;
        $pipe->($arg);
    };


    {  # What does a filter do with a specific data type?
        SEQ => [ 
            filter_seq(@lines), 
            $seq_accu
        ] ,
        ARRAY => [ 
            filter_list(@lines), 
            $list_accu
        ]
    };

}

sub echo {
    if(@_ == 0){
        return 
    }elsif(@_ == 1){
        print "@_"; 
        print "\n"; 
    }elsif(@_ == 3){
        my ($arg, $redir, $fname) = @_;
        if($redir eq '>'){
            open(my $fh, '>', $fname)
                || die "Could not open file '$fname' $!";
            print $fh $arg;
            close $fh;
            return undef;
        }else{
            die "Err(echo): invalid"
        }
    }else{
        die "Err: invalid echo"
    }
}


sub makegen {
    my ($arg ) = @_;

    my $gen =  sub {
        if(@$arg == 0){
            return undef
        }else{
            return shift @$arg
        }
    };
    return bless $gen, 'GENERATOR';
}

sub sh {
    my @r =  `@_` ;
    chomp @r;
    return makegen(\@r);
}

my ($r, $e) = sh q(ls);


sub run {
    my ($it, $fun) = @_;
    my @res;
    while(my $r = $it->()){
        push @res, $r;
    }
    return \@res;
}
sub runloop {
    my ($it, $fun) = @_;
    my @res;
    while(my $r = $it->()){
        push @res, $fun->($r);
    }
    return \@res;
}


sub c {
    my $s = @_;
    my $rec; $rec =  sub {
        my ($hd) = shift;
        if(@_){
            my $sub = $rec->(@_);
            return bless [ $hd,  $sub ]  , 'CONS';
        }else{
            return bless [ $hd, undef ] , 'CONS'
        }
    };
    $rec->(@_);
}


sub flat {
    my ($l ) = @_;
    my $rec; $rec = sub {
        my ($hd, $rest ) = (@_);
        if(defined $rest) {
            my @ehd = (ref $hd eq 'CONS') ? [ $rec->(@$hd) ] : $hd;
            my @erest = ((ref $rest eq 'CONS')or(ref $rest eq 'ARRAY')) ? $rec->(@$rest) : $rest;
            return @ehd, @erest;
        }else{
            my @ehd = (ref $hd eq 'CONS') ? [ $rec->(@$hd) ] : $hd;
            return @ehd;
        }
    };

    return $rec->(@$l);
}


sub filter_ {
    if(@_ == 3){
        die "Err: invalid args" unless ((ref $_[0] eq 'ARRAY') or (ref $_[0] eq 'CODE'));
        die "Err: invalid arg(1)" unless (ref \$_[1] eq 'SCALAR');
        die "Err: invalid arg(2)" unless (ref $_[2] eq 'CODE');
    }else{
        die "dfdjsfd"
        }
}


sub loop {
    my ($input, $fun) = @_;

    if(ref $input eq 'ARRAY'){
        [ map { $fun->($_) } @$input ] 
    }elsif(ref $input eq 'GENERATOR'){
        return sub {
            if(my $r = $input->()){
                return $fun->($r)
            }else{
                return undef
            }
        }
    }else{
        die "Err(loop): inputformat not supported"
    }
}




sub _ls {
    sub {
	my ($dir ) = @_;
	$dir = ($dir) ? $dir : '.';
	opendir(my $dh, $dir) || die "Err: can't opendir $dir: $!";
	my @res;
	while(readdir($dh)){
	    next if $_ eq '.';
	    next if $_ eq '..';
	    push @res, $_
	}
	closedir $dh;
	return \@res;
    }
}

sub dir {
    return cwd() if $_[0] eq '.';
    my @parts =  split '/', $_[0]; 

    my $root = ($parts[0]) ? (shift @parts) : $Root;
    return File::Spec->catdir($root, @parts); 
}

sub cat {
    my $file = $_[0]; 
    my $doc = do {
        local $/ = undef;
        open my $fh, "<", $file
            or die "could not open $file: $!";
        <$fh>;
    };
    print $doc;
}


sub _basename {
    use File::Basename;
    basename (@_);
}
sub apply {
}

sub _sh {
    use IPC::Cmd qw(can_run run);

    return sub {
	### in list context ###
	my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
	    run( command => $_[0], verbose => 0 );
	if( $success ) {
	    print join "", @$full_buf;
	}
    };
}

sub use {
    my ($pkg, $file, $ln ) = caller();

    my %Funcs = (
        echo => \&echo,
        cat => \&cat,
        dir => \&dir, 
        basename => \&_basename,
        ls => \&_ls,
        seq =>\&seq,
        xargs => \&xargs,
        filter => \&filter, 
	apply => \&apply, 
	sh => \&_sh, 
    );

    my @funs;
    foreach(@_){
        if(exists $Funcs{$_}){
	    *{"$pkg\::$_"} = $Funcs{$_}->();
        }else{
            die "Err(import): function $_ not exists"
        }
    }
    return @funs;
}




1;

package Pl::Lisp;
use warnings;
use strict;
use Data::Dumper;

# cons [ line, type, data ]
#
sub lex {
    my $i = shift;

    my %parens = qw| ) ( ] [ } { | ; 

    my $prefix = '';
    my (@tks, @buf, $inside, @pstk);
    foreach (@{$_[0]}) { #lines
        push @tks, { l => $i};
        foreach my $ch (split '', $_){  # chars
            if($inside){
                if($ch eq $inside){
                    push @tks, [ $i, $prefix . $inside,  join '', @buf]; undef @buf;
                    undef $inside; $prefix = '';
                }else{
                    push @buf, $ch
                }
            }else{
                if($ch eq '"'){
                    $inside = $ch;
                }elsif($ch =~ /\:|\#|\'/){
                    $prefix = $ch;
                }else{
                    if( $ch =~ /\[|\(|\{/){
                        if(@buf){push @tks, join '', @buf ; undef @buf;}
                        push(@pstk, $ch); push @tks, $prefix . $ch ; $prefix = '';
                    }elsif( $ch =~ /\]|\)|\}/){
                        die( "Err: unmatching parens") 
                            unless ( $parens{$ch} eq (pop @pstk)) ;
                        if(@buf){push @tks, join '', @buf ; undef @buf;}
                        push @tks, $ch ;
                    }elsif( $ch =~ /\s/){
                        push @buf, $prefix if $prefix; $prefix = '';
                        if(@buf){push @tks, join '', @buf ; undef @buf;}
                    }else{
                        push @buf, $prefix if $prefix; $prefix = '';
                        push @buf, $ch ;
                    }
                }
            }
        }
        $i++;
    }
    push @tks, join '', @buf;
    die "Err: unmatching parens " if @pstk;
    return \@tks
}
sub parse{
    my ($ln, $in) = @_;

    die "Err(parse): premature end" if (@$in == 0 ) ;
    my $tk = shift @$in;

    if(ref $tk eq 'HASH'){  #ln numbers
        # ($ln) = $tk->{ln};
        return parse($ln, $in)
    }elsif(ref $tk eq 'ARRAY'){ #strings
        return $tk;
    }else{
        if($tk =~ /\[|\(|\{/  ){
            my @l ;
            while ((grep { $in->[0]  eq  $_ } qw| ] ) }|) == 0 ){ # not regex 'cause of datatypes
                if(ref $_ eq 'HASH'){
                    ($ln) = $_->{ln};
                }else{
                    push @l, parse($ln, $in);
                }
            }
            shift @$in;
            return [  $ln, $tk, \@l ] ;
            # }elsif($tk =~ /\)|\]/  ){   ########## catched in the lexer
            # die "Err(parse): unexpected token $tk"
        }else{
            return $tk
        }
    }

    
}
sub sexpexpr {

    parse(0, lex( 0, [  (split /\n/, '( ' . $_[0] . ')' ) ] ));
}

sub sexpblock {

    parse(0, lex( 0, [  (split /\n/, '(do ' . $_[0] . ')' ) ] ));
}
sub sexplist {

    parse(0, lex( 0, [  (split /\n/, '[' . $_[0] . ']' ) ] ));
}
sub sexpdict {

    parse(0, lex( 0, [  (split /\n/, '{' . $_[0] . '}' ) ] ));
}

sub l2p_list {
    my ($ctx ,  $data)  = @_;
    return [ map { l2p($ctx, $_) } @$data ]
}
sub l2p_form {
    my ($ctx ,  $data)  = @_;

    my ($hd, @rest) = @$data;
    my ($ehd) =  l2p($ctx, $hd);
    return l2p($ctx, $ehd, @rest);
}
sub l2p_rx{
    my ($ctx ,  $data)  = @_;
    return l2p($ctx, qr($data));
}

sub std_print {
    my ($ctx) = shift; 
    print    @_ if @_;
}


sub std {
    {
        print => \&std_print,
    }
}
sub special {
    {
        do => \&l2p_block 
    }
}

sub ctx {
    my $syntax = {
        '(' => \&l2p_form,
        '[' => \&l2p_list,
        '#"' => \&l2p_rx,
        '"' => sub { return $_[1] } 
    };
    return {
            lib => { 
                syntax => $syntax,
                special => special(), 
            }, 
            normal => [{}, {}, std(), ],
            quote => 0, 
    };
}
sub rslv{
    my ($ctx, $in) = @_;

    if(exists $ctx->{lib}{special}{$in}){
        return sub {
            $ctx->{lib}{special}{$in}->(@_)
        };
    }else{
        my $res;
        foreach (@{$ctx->{normal}}){
            if(exists $_->{$in}){
                $res = $_->{$in};
                last
            }
        }
        if($res) {
             sub {
                 my $ctx = shift;
                $res->($ctx,  map { l2p($ctx, $_) } @{ $_[0] } )
            }
        }else{
             die "Err(rslv): couldnt resolv $in";
         }
    }
}
sub l2p_block {
    my ($ctx, $in) = @_;

    my $nctx = { %$ctx };
    my @res;
    foreach (@$in) {
        push @res, l2p($nctx, $_)
    }
    return pop @res;
}
sub l2p {
    my ($ctx, $in ) = (shift, shift);

    if(ref $in eq 'ARRAY'){
        my ($ln, $syn, $data) = @{ $in};
        $ctx->{ln} = $ln;
        return ($ctx->{quoted})
            ? die 'todo'
            : $ctx->{lib}{syntax}{$syn}->($ctx, $data);
    }elsif(ref \$in eq 'SCALAR'){ #symbol?
        return rslv($ctx, $in);
    }elsif(ref $in eq 'CODE'){ #symbol?
        return $in->( $ctx, \@_);
    }else{
        return $in;
    }

}

1;
